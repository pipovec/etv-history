module.exports = {
    filenameHashing: false,
    configureWebpack:{
        optimization: {
          splitChunks: {
            minSize: 10000,
            maxSize: 250000,
          }
        }
    }
}