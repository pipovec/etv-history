import { createStore } from 'vuex'
import axios from 'axios'
import "../models/formConstants"

const API_URL = 'https://api2.fpcstat.cz/api';

export default createStore({
  state: {
    tanks: [] ,
    tank: {},
    tankSelected: false,
    etvIsLoading: false,
    etvValue: [],
  },
  mutations: {
    SET_TANKS(state, data) {
      state.tanks = data;
    },
    SET_TANK(state, tankData) {
      state.tank = tankData;
    },
    SET_TANK_SELECTED(state, payload) {
      state.tankSelected = payload;
    },
    SET_ETV_VALUE(state, data) {
      state.etvValue = data;
    },
    SET_ETV_IS_LOADING(state, status: boolean) {
      state.etvIsLoading = status;
    },
  },
  getters: {
    IS_TANK_SELECTED(state) {
      return state.tankSelected;
    },
    GET_TANK(state) {
      return state.tank;
    },
    GET_TANKS(state) {
      return state.tanks;
    },
  },
  actions: {
    async GET_TANKS({ commit }, postData) {
      await axios.get(API_URL + '/encyclopedia_vehicles?' + postData,  {
        headers: {
          'Accept': 'application/json',
        }
      }).then(response => {
        commit('SET_TANKS', response.data);
      })
    },

    async GET_ETV_VALUE( { commit }, tankId: bigint) {
      commit('SET_ETV_IS_LOADING', true);
      await axios(API_URL+ '/expected_tank_value_histories?itemsPerPage=365&date[after]=2023-08-01&order[date]=asc&tank_id=' + tankId, {
        headers: {
          'Accept': 'application/json',
        }
      }).then(response => {
        commit('SET_ETV_IS_LOADING', false);
        commit('SET_ETV_VALUE', response.data);
      });
    },
  },
})
