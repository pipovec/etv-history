export interface ITankValues {
    big_icon: string,
    name: string,
    tank_id: number,
}