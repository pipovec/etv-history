import { IFormValues } from '@/models/formValues'

export default {
    formConstant: {
        level: [10, 9, 8, 7, 6, 5, 4, 3, 2, 1],
        nation: ['ussr', 'usa', 'germany', 'poland', 'japan', 'france', 'italy', 'sweden', 'czech', 'china', 'uk'],
        tankType: ['mediumTank', 'heavyTank', 'lightTank', 'AT-SPG', 'SPG'],

    } as IFormValues,
}; 

