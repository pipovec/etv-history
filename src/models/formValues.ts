export interface IFormValues {
  level: number[];
  nation: string[];
  tankType: string[];
}